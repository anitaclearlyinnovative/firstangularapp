/**
 * Created by anitahall on 1/8/16.
 */
angular.module('MyDataService' ,[])

.service('MyDataService', function ($q) {
  // Setup data
  //push is just a way to put the data into the array you do not need
  //to call push
  var itemsList = [];
  itemsList.push({
    "id"  : "0",
    "name": "Barot Bellingham",
    "shortname": "Barot_Bellinghame",
    "reknown": "Royal Academy of Painting and Sculpture",
    "bio": "Barot has just finished his final year at The Royal Academy of Whatever"
  });
  itemsList.push({
    "id"  : "1",
    "name": "Anita Hall",
    "shortname": "Anita_Hall",
    "reknown": "Developer Training at Clearly Innovative",
    "bio": "Anita is a development apprentice working toward the goal of junior developer"
  });
  itemsList.push({
    "id"  : "2",
    "name": "Hillary Hewitt Goldwynn-Post",
    "shortname": "Hillary_Goldwynn",
    "reknown": "New York University",
    "bio": "Hillary is a sophmore art sculpture student at New York Univeristy"
  });
  itemsList.push({
    "id"  : "3",
    "name": "Kadijah Loafer",
    "shortname": "Dej Loaf",
    "reknown": "Rap City University ",
    "bio": "Oh yah yah, back up off me"
  });
  itemsList.push({
    "id"  : "4",
    "name": "Barot Bellingham",
    "shortname": "Barot_Bellinghame",
    "reknown": "Royal Academy of Painting and Sculpture",
    "bio": "Barot has just finished his final year at The Royal Academy of Whatever"
  });
  itemsList.push({
    "id"  : "5",
    "name": "Anita Hall",
    "shortname": "Anita_Hall",
    "reknown": "Developer Training at Clearly Innovative",
    "bio": "Anita is a development apprentice working toward the goal of junior developer"
  });
  itemsList.push({
    "id"  : "6",
    "name": "Hillary Hewitt Goldwynn-Post",
    "shortname": "Hillary_Goldwynn",
    "reknown": "New York University",
    "bio": "Hillary is a sophmore art sculpture student at New York Univeristy"
  });
  itemsList.push({
    "id"  : "7",
    "name": "Kadijah Loafer",
    "shortname": "Dej Loaf",
    "reknown": "Rap City University ",
    "bio": "Oh yah yah, back up off me"
  });
  // these are functions exposed to public
  return {
    /**
     * returns all of the data
     */
    getAllItems: function () {
      var deferred = $q.defer();
      setTimeout(function () {
        deferred.resolve(itemsList);
      }, 500);
      return deferred.promise;
    },
    /**
     * Gets item by specific id
     * @param   {Number}  _itemId index in the array
     * @returns {Promise}
     */
    getItemById: function (_itemId) {
      var deferred = $q.defer();
      setTimeout(function () {
        if (_itemId > itemsList.length || (_itemId < 0)) {
          deferred.reject("Invalid Object Id");
        } else {
          deferred.resolve(itemsList[_itemId]);
        }
      }, 1000);
      return deferred.promise;
    }
  }

});

