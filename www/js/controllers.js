angular.module('starter-controller', ['MyDataService'])

  .controller('homeCtrl', function ($scope, $state, MyDataService) {
    // body...
    $scope.stateInfo = $state.current;
    $scope.pageTitle = "Home Page";
    //detailClick- is a function and when you click the button o
    $scope.detailClick = function (item) {
      //store index in a variable
      var id = item.id;
      $state.go("detail", {id: id});
    };
    function populateList() {

      MyDataService.getAllItems().then(function (_data) {
        $scope.itemsList = _data;
      });
    }
    //populateList-MyDataService gets the data and assigns it to the itemsList
    // To initialize the view, call populateList when controller
    // starts
    populateList();

  })

  // the controllers for each of the states, we are “injecting” the
  // $scope and the specific $state information, these are angular
  // services provided as part of the framework
  .controller('detailCtrl', function ($scope,$state,person) {
    $scope.person = person;
  });
